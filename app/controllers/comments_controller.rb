class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comment_params)
    redirect_to post_path(@post)
  end

  def new
  	@comment = Comment.new
  end

  def index
  	@comments = Comment.all
  end

	def edit
		@comment = Comment.find(params[:id])
	end  

	def update
	  @comment = Comment.find(params[:id])
	 
	  if @comment.update(comment_params)
	    redirect_to @comment
	  else
	    render 'edit'
	  end
	end		

	def show
		@comment = Comment.find(params[:id])
	end 

  private
    def comment_params
      params.require(:comment).permit(:body)
    end
end